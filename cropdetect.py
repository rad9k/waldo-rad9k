import cv2
import sys
import os
import numpy as np
import math

class Detector:

    def __init__(self):

        if(len(sys.argv)<3):
            self.printHelp()
            print("ERROR: please specify first and second image names")
            print()
            self.errorExit()

        self.doDebug = False

        self.i1Name = sys.argv[1]
        self.i2Name = sys.argv[2]

        self.treshold = 100

        if len(sys.argv) >= 5 and sys.argv[3]=="-t":
              try:
                 self.treshold = int(sys.argv[4])
              except:
                print("ERROR: can not parse treshold value")
                print()


    def debug(self, str):
    
        if self.doDebug:
            print(str)    


    def printHelp(self):
   
        print("cropdetector (c) radoslaw tereszczuk for waldo (https://waldophotos.com/)")
        print()
        print("Usage:")
        print()
        print("  cropdetector <firstImageName.jpg> <secondImageName.jpg> [options]")
        print()
        print("Options:")
        print()
        print("-t <treshold>    Sets treshold level to given value.")
        print()


    def run(self):
       
        self.loadImages()

        self.prepareImages()
        self.prepareTemplateImage()
        self.prepareTryImage()

        self.process()

         
    def loadImages(self):

        self.i1Image = cv2.imread(self.i1Name) 
        self.i2Image = cv2.imread(self.i2Name)

        if self.i1Image is None:
            print("ERROR: can not open " + self.i1Name)
            print()
            self.errorExit()

        if self.i2Image is None:
            print("ERROR: can not open " + self.i2Name)
            print()
            self.errorExit()

        self.i1Image = np.array(self.i1Image, dtype = np.int16) # need to do it for matrix subtract to work properly
        self.i2Image = np.array(self.i2Image, dtype = np.int16)


    def prepareImages(self):

        i1Width, i1Height, i1Depth = self.i1Image.shape
        i2Width, i2Height, i2Depth = self.i2Image.shape

        if i1Width > i2Width and i1Height > i2Height: # check what image is larger
            self.tryImage = self.i1Image
            self.templateImage = self.i2Image
        elif i1Width <= i2Width and i1Height <= i2Height:
            self.tryImage = self.i2Image
            self.templateImage = self.i1Image
        else: # crop not possible
            print("no crop")


    def prepareTemplateImage(self): # execute extendImage and prepareLowerResolutionImages for template image. we store orginal / extended image ratio also
        
        self.orginalTemplateWidth, self.orginalTemplateHeight, depth = self.templateImage.shape # need to colect before image extension

        self.templateImageListCount, self.templateImage = self.extendImage(self.templateImage)

        templateWidth, templateHeight, depth = self.templateImage.shape

        self.templateWidthRatio = self.orginalTemplateWidth / templateWidth 

        self.templateHeightRatio = self.orginalTemplateHeight / templateHeight

        self.templateImageList = self.prepareLowerResolutionImages(self.templateImage, self.templateImageListCount)


    def prepareTryImage(self): # execute extendImage and prepareLowerResolutionImages for try image

        self.tryImageListCount, self.tryImage = self.extendImage(self.tryImage)
        self.tryImageList = self.prepareLowerResolutionImages(self.tryImage, self.tryImageListCount)


    def extendImage(self, image): # extend orginal image to square of longer orginal image dimention length. fill the rest with zeros

        width, height, depth = image.shape

        # select longer dimention for try image

        if width < height:
            longerDimention = height
        else:
            longerDimention = width

        self.debug("width: " + str(width) + " height: " + str(height) + " longerDimention: " + str(longerDimention))

        # compute new size (being power of 2) and number of lower resolution images (+ 1 for orginal resolution) same time

        imageListCount = 0 # number of lower resolution images (+ 1 for orginal resolution)
        newSize = 1
        while True:
            imageListCount += 1
            newSize = newSize * 2
            if newSize >= longerDimention:
                break

        self.debug("newSize: " + str(newSize) + " imageListCount: " + str(imageListCount))

        # extend the try with zeros

        image = np.hstack(( image, np.zeros((width, newSize - height, depth)) ))

        image = np.vstack(( image, np.zeros((newSize - width, newSize, depth)) ))

        return imageListCount, image

  
    def prepareLowerResolutionImages(self, image, imageListCount): # prepare lower resolution image list

        width, height, depth = image.shape

        imageList = list()
        imageList.append(image)

        for c in range(imageListCount - 1, 0, -1):

            topImageFromList = imageList[len(imageList) - 1]

            dimension = np.power(2, c)
            newImage = np.zeros((dimension, dimension, depth))
         
            for x in range(dimension):
                for y in range(dimension):
                        newImage[y, x] = (topImageFromList[y * 2, x * 2] \
                        + topImageFromList[ (y * 2) + 1, x * 2] \
                        + topImageFromList[ (y * 2), (x * 2) + 1] \
                        + topImageFromList[ (y * 2) + 1, (x * 2) + 1]) / 4

            imageList.append(newImage)

        return imageList


    def patternMatch(self, imageLevel, tryXStart, tryXEnd, tryYStart, tryYEnd): # reccurent pattern match

        beg = ""
        for xx in range(6 - imageLevel):
            beg +="\t"

        self.debug(beg + "*PATTERN MATCH* imageLevel: " + str(imageLevel) + " tryXStart:" + str(tryXStart) +" tryXEnd: " + str(tryXEnd) + " tryYStart: " + str(tryYStart) + " tryYEnd: " + str(tryYEnd))
        i=imageLevel
        tryImage = self.tryImageList[imageLevel]
        templateImage = self.templateImageList[imageLevel]

        templateWidth, templateHeight, templateDepth = templateImage.shape
        templateWidth = math.floor(templateWidth * self.templateWidthRatio)
        templateHeight = math.floor(templateHeight * self.templateHeightRatio)

        tryWidth, tryHeight, tryDepth = tryImage.shape

        if tryXStart < 0:
            tryXStart = 0

        if tryYStart < 0:
            tryYStart = 0

        if tryXEnd > tryWidth:
            tryXEnd = tryWidth

        if tryYEnd > tryHeight:
            tryYEnd = tryHeight
        
        self.debug("template: width " + str(templateWidth) + " height " + str(templateHeight) + " depth " + str(templateDepth))
        self.debug("try: width " + str(tryWidth) + " height " + str(tryHeight) + " depth " + str(tryDepth))

        tryXTestEnd = tryXEnd - templateWidth
        tryYTestEnd = tryYEnd - templateHeight

        self.debug("test end: width " + str(tryXTestEnd) + " height " + str(tryYTestEnd))

        for tryX in range(tryXStart, tryXTestEnd):
            for tryY in range(tryYStart, tryYTestEnd):
                for templateY in range(templateHeight):
                    for templateX in range(templateWidth):

                        rDiff, gDiff, bDiff = np.abs(tryImage[tryY + templateY, tryX + templateX] - templateImage[templateY, templateX])
                        
                        if rDiff > self.treshold or gDiff > self.treshold or bDiff > self.treshold:
                            break
                    
                    if templateX < templateWidth - 1:
                        break
                if(templateY == (templateHeight - 1) and templateX == (templateWidth - 1)):
                    if(imageLevel == 0):
                        return tryX, tryY
                    else:
                        squereSize = math.pow(2, len(self.tryImageList) - imageLevel + 10)

                        foundX, foundY = self.patternMatch(imageLevel - 1, \
                        int((tryX * 2) - squereSize), int(tryX * 2 + squereSize) - 1,\
                        int((tryY * 2) - squereSize), int(tryY * 2 + squereSize) - 1)

                        if foundX != -1:
                            return foundX, foundY
                                    
        return -1, -1 # not found
                   
                
    def process(self):

        startLevel = len(self.templateImageList) - 1

        templateWidth, templateHeight, templateDept = self.templateImageList[startLevel].shape # 

        templateWidth = math.floor(templateWidth * self.templateWidthRatio)
        templateHeight = math.floor(templateHeight * self.templateHeightRatio)
        
        while templateWidth < 1 or templateHeight < 1: # need to have min 1 size in both dimensions for template
            startLevel -= 1
            templateWidth, templateHeight, templateDept = self.templateImageList[startLevel].shape

            templateWidth = math.floor(templateWidth * self.templateWidthRatio)
            templateHeight = math.floor(templateHeight * self.templateHeightRatio)

        tryImage = self.tryImageList[startLevel]

        tryWidth, tryHeight, tryDepth = tryImage.shape

        retX, retY = self.patternMatch(startLevel, 0, tryWidth, 0, tryHeight) # run reccurent search

        if(retX == -1):
            print("crop not in the image")
        else:
            print("found crop at " + str(retX) + " " + str(retY))

    @staticmethod
    def printImage(i):
        w,h,d = i.shape

        for y in range(h):
            for x in range(w):
                print(str(x) + "," + str(y) + " " +  str(i[y,x]), end = "")
            print()


    def errorExit(self):
        
        try:
            sys.exit(1)
        except SystemExit as e:    
            os._exit(1) 
            

def main():
    instance = Detector()
    instance.run()

if __name__ == "__main__":
    main()

